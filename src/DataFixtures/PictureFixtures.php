<?php


namespace App\DataFixtures;


use App\Entity\Picture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PictureFixtures extends Fixture
{

    /**
     * @param ObjectManager $manager
     *
     */
    public function load(ObjectManager $manager)
    {


        $pictures = [
            "https://images.fineartamerica.com/images/artworkimages/mediumlarge/2/1-no-face-my-neighbor-totoro-valentina-hramov.jpg",
            "http://courte-focale.fr/Images/Le%20Voyage%20de%20Chihiro_6.jpg",
            "https://2.bp.blogspot.com/-3KgyvnbTfE8/UqIoXYzOtmI/AAAAAAAACSY/EY7R3gsOBqg/s640/6b175d18d2f7f1369c6b0f934667202e.jpg"

        ];

        foreach ($pictures as $picture) {
            $pictureL = new Picture();
            $pictureL->setLinkPicture($picture);
            $manager->persist($pictureL);
        }
        $manager->flush();


    }
}
