<?php


namespace App\DataFixtures;


use App\Entity\Dog;
use App\Repository\BreedRepository;
use App\Repository\PictureRepository;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class DogFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var BreedRepository
     */
    private $breedRepository;
    /**
     * @var PictureRepository
     */
    private $pictureRepository;

    /**
     * DogFixtures constructor.
     * @param BreedRepository $breedRepository
     * @param PictureRepository $pictureRepository
     */



    public function __construct(BreedRepository $breedRepository , PictureRepository $pictureRepository)
    {
        $this -> breedRepository = $breedRepository;
        $this -> pictureRepository = $pictureRepository;
    }


    /**
     * @param ObjectManager $manager
     *
     */
    public function load(\Doctrine\Persistence\ObjectManager $manager)
    {
        $sansVisage = $this ->breedRepository ->findOneBy( [
            "name" => "Esprit sans visage"
        ]);
        $espritRiver = $this ->breedRepository ->findOneBy( [
            "name" => "Esprit de la rivière"
        ]);
        $espritForest = $this ->breedRepository ->findOneBy( [
            "name" => "Esprit de la forêt"
        ]);

        $sansVisagePicture = $this ->pictureRepository ->findOneBy( [
            "linkPicture" => "https://images.fineartamerica.com/images/artworkimages/mediumlarge/2/1-no-face-my-neighbor-totoro-valentina-hramov.jpg"
        ]);
        $espritRiverPicture = $this ->pictureRepository ->findOneBy( [
            "linkPicture" => "http://courte-focale.fr/Images/Le%20Voyage%20de%20Chihiro_6.jpg"

        ]);

        $espritForestPicture = $this ->pictureRepository ->findOneBy( [
            "linkPicture" => "https://2.bp.blogspot.com/-3KgyvnbTfE8/UqIoXYzOtmI/AAAAAAAACSY/EY7R3gsOBqg/s640/6b175d18d2f7f1369c6b0f934667202e.jpg"

        ]);

        $date = new DateTime('1888-06-06');

        $noFace = new Dog();
        $noFace->setName('Sans Visage');
        $noFace->setDescription('Personnage mystérieux et masqué, il peut disparaître à sa convenance. De nature timide, il a besoin d\'un temps d\'adaptation avant de se sentir comme un membre entier de la famille. Temps que vous ne regretterez pas');
        $noFace->setDateBirth($date);
        $noFace->addDogBreed($sansVisage);
        $noFace->addDogPicture($sansVisagePicture);
        $noFace->setIsAdopted(0);
        $manager->persist($noFace);

        $kamy = new Dog();
        $kamy->setName('Kawa No Kami');
        $kamy->setDescription('Ancien dieu d\'une rivière polluée par l\'homme. Etonnant par ses différentes formes');
        $kamy->setDateBirth($date);
        $kamy->addDogBreed($espritRiver);
        $kamy->addDogPicture($espritRiverPicture);
        $kamy->setIsAdopted(0);
        $manager->persist($kamy);

        $totoro = new Dog();
        $totoro->setName('Totoro');
        $totoro->setDescription('Totoro est un animal mythique, à l\'apparence d\'un énorme raton qui rappelle à la fois un ours, une chouette ou encore un tanuki.');
        $totoro->setDateBirth($date);
        $totoro->addDogBreed($espritForest);
        $totoro->addDogPicture($espritForestPicture);
        $totoro->setIsAdopted(0);
        $manager->persist($totoro);
        $manager->flush();


    }

    /**
     * @return mixed
     */
    public function getDependencies()
    {
       return[
           BreedFixture::class,
           PictureFixtures::class,
           ];
    }
}
