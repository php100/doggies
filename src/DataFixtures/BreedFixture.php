<?php


namespace App\DataFixtures;


use App\Entity\Breed;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BreedFixture extends Fixture
{

    /**
     * @param ObjectManager $manager
     *
     */
    public function load(\Doctrine\Persistence\ObjectManager $manager)
    {
        $breeds = [
            ["nom" => "Esprit sans visage",
            "description" =>"Type d'esprit se promenant seul, sans visage, attention beaucoup d'appétit"],
            ["nom" => "Esprit de la rivière",
                "description" =>"Vivant sous l'eau, mais aussi sous les marais, très bon compagnon dans des régions humides"],
            ["nom" => "Esprit de la forêt",
                "description" =>"A mis chemin entre la forêt et la ville, ce type d'esprit trouvera sa place dans des familles avec enfants"],
        ];

        foreach ($breeds as $breed){
           $breedBD = new Breed();
            $breedBD->setDescription($breed["description"]);
            $breedBD->setName($breed["nom"]);
            $manager->persist($breedBD);

        }
        $manager->flush();
    }
}
