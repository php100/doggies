<?php


namespace App\DataFixtures;


use App\Entity\Advert;
use App\Repository\DogRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AdvertFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * @var DogRepository
     */
    private $dogRepository;

    /**
     * AdvertFixture constructor.
     * @param DogRepository $dogRepository
     */
    public function __construct(DogRepository $dogRepository)
    {
        $this-> dogRepository = $dogRepository;
    }


    /**
     * @param ObjectManager $manager
     *
     */
    public function load(\Doctrine\Persistence\ObjectManager $manager)
    {
        $dogs = $this->dogRepository->findAll();
//        $random = mt_rand(0,count($dogs) - 1);
//        $randomDog = $dogs[$random];

        for ($count = 0; $count < 10; $count++) {
            $advert = new Advert();
            $advert ->setTitle("Titre " . $count);
            $advert ->setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse convallis tristique lobortis. Nunc mauris odio, finibus at mattis in, fermentum at nisl. Morbi ultrices convallis tortor quis mattis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Ut molestie vel enim eu bibendum. Aliquam elementum, est vel ultricies pulvinar, sapien dolor accumsan tortor, eget cursus neque tellus in quam. Nullam interdum, odio eget condimentum congue, felis nisi dignissim odio, vitae ultrices elit diam id risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla erat dolor, semper sed posuere pharetra, elementum rhoncus justo. Vestibulum ut erat accumsan, eleifend ante vitae, ullamcorper felis. Etiam interdum tortor eget leo mattis facilisis. Maecenas mollis nisi in justo lacinia, at facilisis neque ornare. Phasellus vitae pharetra mi. Maecenas ut aliquet erat. Cras vehicula sit amet neque ac fringilla.");
            $advert->setIsFeeded(0);
            $random = mt_rand(0,count($dogs) - 1);
            $randomDog = $dogs[$random];
            $advert->addDog($randomDog);
            $manager->persist($advert );
        }

        $manager->flush();

    }




    /**
     * @return mixed
     */
    public function getDependencies()
    {
        return[
            DogFixtures::class,

        ];
    }


}
