<?php

namespace App\Entity;

use App\Repository\PictureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PictureRepository::class)
 */
class Picture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $linkPicture;

    /**
     * @ORM\ManyToOne(targetEntity=Dog::class, inversedBy="dogPicture")
     */
    private $pictureDog;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLinkPicture(): ?string
    {
        return $this->linkPicture;
    }

    public function setLinkPicture(string $linkPicture): self
    {
        $this->linkPicture = $linkPicture;

        return $this;
    }

    public function getPictureDog(): ?Dog
    {
        return $this->pictureDog;
    }

    public function setPictureDog(?Dog $pictureDog): self
    {
        $this->pictureDog = $pictureDog;

        return $this;
    }
}
