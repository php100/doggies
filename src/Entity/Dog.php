<?php

namespace App\Entity;

use App\Repository\DogRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DogRepository::class)
 */
class Dog
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     */
    private $dateBirth;



    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Advert::class, inversedBy="dogs")
     */
    private $advert;

    /**
     * @ORM\OneToMany(targetEntity=Picture::class, mappedBy="pictureDog")
     */
    private $dogPicture;

    /**
     * @ORM\ManyToMany(targetEntity=Breed::class, mappedBy="breedDog")
     */
    private $dogBreed;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAdopted;

    public function __construct()
    {
        $this->dogPicture = new ArrayCollection();
        $this->dogBreed = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDateBirth(): ?\DateTimeInterface
    {
        return $this->dateBirth;
    }

    public function setDateBirth(\DateTimeInterface $dateBirth): self
    {
        $this->dateBirth = $dateBirth;

        return $this;
    }



    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAdvert(): ?Advert
    {
        return $this->advert;
    }

    public function setAdvert(?Advert $advert): self
    {
        $this->advert = $advert;

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getDogPicture(): Collection
    {
        return $this->dogPicture;
    }

    public function addDogPicture(Picture $dogPicture): self
    {
        if (!$this->dogPicture->contains($dogPicture)) {
            $this->dogPicture[] = $dogPicture;
            $dogPicture->setPictureDog($this);
        }

        return $this;
    }

    public function removeDogPicture(Picture $dogPicture): self
    {
        if ($this->dogPicture->removeElement($dogPicture)) {
            // set the owning side to null (unless already changed)
            if ($dogPicture->getPictureDog() === $this) {
                $dogPicture->setPictureDog(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Breed[]
     */
    public function getDogBreed(): Collection
    {
        return $this->dogBreed;
    }

    public function addDogBreed(Breed $dogBreed): self
    {
        if (!$this->dogBreed->contains($dogBreed)) {
            $this->dogBreed[] = $dogBreed;
            $dogBreed->addBreedDog($this);
        }

        return $this;
    }

    public function removeDogBreed(Breed $dogBreed): self
    {
        if ($this->dogBreed->removeElement($dogBreed)) {
            $dogBreed->removeBreedDog($this);
        }

        return $this;
    }

    public function getIsAdopted(): ?bool
    {
        return $this->isAdopted;
    }

    public function setIsAdopted(bool $isAdopted): self
    {
        $this->isAdopted = $isAdopted;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }



}
