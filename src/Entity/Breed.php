<?php

namespace App\Entity;

use App\Repository\BreedRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BreedRepository::class)
 */
class Breed
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=Dog::class, inversedBy="dogBreed")
     */
    private $breedDog;

    public function __construct()
    {
        $this->breedDog = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Dog[]
     */
    public function getBreedDog(): Collection
    {
        return $this->breedDog;
    }

    public function addBreedDog(Dog $breedDog): self
    {
        if (!$this->breedDog->contains($breedDog)) {
            $this->breedDog[] = $breedDog;
        }

        return $this;
    }

    public function removeBreedDog(Dog $breedDog): self
    {
        $this->breedDog->removeElement($breedDog);

        return $this;
    }

    public function __toString()
    {
        return $this->name;
     }
}
