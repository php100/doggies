<?php

namespace App\Entity;

use App\Repository\AdvertRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=AdvertRepository::class)
 */
class Advert
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Contact::class, mappedBy="advertContacted")
     */
    private $contactsForAnnonce;

    /**
     * @ORM\OneToMany(targetEntity=Dog::class, mappedBy="advert")
     */
    private $dogs;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isFeeded;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

    public function __construct()
    {
        $this->contactsForAnnonce = new ArrayCollection();
        $this->dogs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContactsForAnnonce(): Collection
    {
        return $this->contactsForAnnonce;
    }

    public function addContactsForAnnonce(Contact $contactsForAnnonce): self
    {
        if (!$this->contactsForAnnonce->contains($contactsForAnnonce)) {
            $this->contactsForAnnonce[] = $contactsForAnnonce;
            $contactsForAnnonce->setAdvertContacted($this);
        }

        return $this;
    }

    public function removeContactsForAnnonce(Contact $contactsForAnnonce): self
    {
        if ($this->contactsForAnnonce->removeElement($contactsForAnnonce)) {
            // set the owning side to null (unless already changed)
            if ($contactsForAnnonce->getAdvertContacted() === $this) {
                $contactsForAnnonce->setAdvertContacted(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Dog[]
     */
    public function getDogs(): Collection
    {
        return $this->dogs;
    }

    public function addDog(Dog $dog): self
    {
        if (!$this->dogs->contains($dog)) {
            $this->dogs[] = $dog;
            $dog->setAdvert($this);
        }

        return $this;
    }

    public function removeDog(Dog $dog): self
    {
        if ($this->dogs->removeElement($dog)) {
            // set the owning side to null (unless already changed)
            if ($dog->getAdvert() === $this) {
                $dog->setAdvert(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
       return $this->getTitle();
    }

    public function getIsFeeded(): ?bool
    {
        return $this->isFeeded;
    }

    public function setIsFeeded(?bool $isFeeded): self
    {
        $this->isFeeded = $isFeeded;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

}
