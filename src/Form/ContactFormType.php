<?php

namespace App\Form;

use App\Entity\Advert;
use App\Entity\Contact;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subject', TextType::class, [
                'required' => true,
                'trim' => true,
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'trim' => true,
            ])
            ->add('message', TextareaType::class, [
                'required' => true,
                'trim' => true,
            ])
            ->add('advertContacted', EntityType::class, [
                'class' => Advert::class,
                'required' => false,
//                'choice_label' => 'title', à la place de ça on utilise un toString dans la classe Advert
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
