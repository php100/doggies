<?php

namespace App\Controller\Admin;

use App\Entity\Breed;
use App\Entity\Contact;
use App\Entity\Dog;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin_dashboard")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(AdminUrlGenerator::class);
        $linkToDogsAdmin = $routeBuilder->setController(DogCrudController::class)->generateUrl();
        $linkToContactAdmin = $routeBuilder->setController(ContactCrudController::class)->generateUrl();
        return $this->render('admin/dashboard.html.twig',[

            'linkToDogsAdmin' => $linkToDogsAdmin,
            'linkToContactAdmin' =>$linkToContactAdmin
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Adopte un spirit');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Message', 'fas fa-list', Contact::class);
        yield MenuItem::linkToCrud('Les esprits', 'fas fa-list', Dog::class);
        yield MenuItem::linkToCrud('Les races', 'fas fa-list', Breed::class);
    }
}
