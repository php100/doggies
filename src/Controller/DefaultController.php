<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Entity\Contact;
use App\Form\ContactFormType;
use App\Repository\AdvertRepository;
use App\Repository\ContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class DefaultController extends AbstractController
{

    /**
     * @var ContactRepository
     */
    private $contactRepository;

    /**
     * DefaultController constructor.
     * @param ContactRepository $contactRepository
     */
    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }


    /**
     * @Route("/", name="index")
     * index est une action de ce DefaultController
     * lié à un chemin et donne une réponse
     */
    public function index(AdvertRepository $advertRepository): Response
    {
        $adverts = $advertRepository->findAdvertNotAdopted(4,'updated');
        return $this->render('view/index.html.twig', [
            'adverts' => $adverts,
        ]);
    }



    /**
     * @Route("/contact", name="contact")
     * @Route("/contact/{id}", name="contact_for_adoption")
     *
     */
    public function contact(Request $request, EntityManagerInterface $entityManager, Advert $advert = null): Response
    {   $contact = new Contact();
    //formulaire et l'objet que l'on veut mettre a jour grâce au formulaire
         $form = $this->createForm(ContactFormType::class, $contact);

         $form->handleRequest($request); // on prend les données POST quand le formulaire est fournis et les mettre dans contact
        // si formulaire est valide on registre
         if ($form->isSubmitted() && $form->isValid()){
//             dd($contact);
             $entityManager->persist($contact);
             $entityManager->flush();

             $this->addFlash('success', 'Votre message a bien été envoyé');

             return $this->redirectToRoute('contact');
         }

        return $this->render('view/contact.html.twig',[
            'form' => $form->createView(),
    ]);




    }

    /**
     * @Route("/condition", name="condition")
     */
    public function condition(): Response
    {
        return $this->render('view/condition.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

}
