<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Repository\AdvertRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/adoption")
 */
class AnnonceController extends AbstractController
{
    /**
     * @Route("/", name="adoption_all",  methods={"GET"})
     * @param AdvertRepository $advertRepository
     * @return Response
     */
    public function index(AdvertRepository $advertRepository): Response
    {
        return $this->render('annonce/adoptions.html.twig', [
            'adverts' => $advertRepository->findAll(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="adoption_detail", methods={"GET"})
     * @param Advert $advert
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function show(Advert $advert, EntityManagerInterface $entityManager): Response
    {
        return $this->render('annonce/detail.html.twig', [
            'advert' => $advert
        ]);
    }





}
