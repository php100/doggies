<?php


namespace App\Factory;


use App\Repository\ContactRepository;

class ContactFactory
{
    /**
     * @var ContactRepository
     */
    private $contactRepository;


    /**
     * ContactFactory constructor.
     */
    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }
}
