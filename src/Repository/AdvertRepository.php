<?php

namespace App\Repository;

use App\Entity\Advert;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Advert|null find($id, $lockMode = null, $lockVersion = null)
 * @method Advert|null findOneBy(array $criteria, array $orderBy = null)
 * @method Advert[]    findAll()
 * @method Advert[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdvertRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Advert::class);
    }

// on passe des paramètres pour dire que l'on veut 4 annonces classé par date croissante
// et on renvoit toujours un tableau avec le array
public function findAdvertNotAdopted (int $number = 0,  string $orderBy = null): array
    {

        //$qb objet queryBuilder avec l'alias a de advert
        $qb = $this->createQueryBuilder('a')
            //on joint la table dogs alias b
            ->innerJoin('a.dogs', 'd')
            ->andWhere('d.isAdopted = :isAdopted')
            ->setParameter('isAdopted', false);
        // si mon number est different de 0 en paramètre on lui dit d'afficher le nb de resultat passé en param
        if ($number > 0){
            $qb->setMaxResults($number);
        }
        $qb->orderBy('a.title', 'ASC');
        //et si le orderBy en param est différent de null c'est là qu'on gère de les classé en décroissant
        if (!empty($orderBy)){
            $qb->orderBy('a.title', 'DESC');
        }

        return $qb->getQuery()->getResult();

    }

//    public function findAdvertNotAdopted (int $number = 0,  string $orderBy = null): array
//    {
//
//        //$qb objet queryBuilder avec l'alias a de advert
//        $qb = $this->createQueryBuilder('a')
//            //on joint la table dogs alias b
//            ->innerJoin('a.dogs', 'd')
//            ->andWhere('d.isAdopted = :isAdopted')
//            ->setParameter('isAdopted', false)
//            ->orderBy('a.title', 'ASC');
//
//        return $qb->getQuery()->getResult();
//
//    }

    // /**
    //  * @return Advert[] Returns an array of Advert objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Advert
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
